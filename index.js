'use strict'

module.exports.info = message => {
  console.info(`${date()} [ INFO ] ${message}`)
}

module.exports.debug = message => {
  console.info(`${date()} [ DEBUG ] ${message}`)
}

module.exports.error = message => {
  console.info(`${date()} [ ERROR ] ${message}`)
}

module.exports.request = message => {
  console.info(`${date()} [ REQUEST ] ${message}`)
}

const date = () => {
  const date = new Date()
  return `${date.getDay()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`
}
